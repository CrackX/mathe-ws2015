import java.util.ArrayList;
import java.util.Random;

public class MonteCarloMain {

	final static int MIN_COPIES = 0;
	final static int MAX_COPIES = 1300;
	final static int TOO_MUCH_COPIES_COSTS = 1;
	final static int TOO_LITTLE_COPIES_COSTS = 10;
	final static int EXPECTATION_VAL = 560;
	final static int STD_DEVIATION = 200;
	final static int MAX_RUNS = 100000;
	static Random rnd = new Random();

	
	public static void main(String[] args) {

		//generate result interval 
		int minVal = Integer.MAX_VALUE; int maxVal = Integer.MIN_VALUE;
		int runs = 20;
		int sum = 0; int avg;
		int interval[] = new int[runs];
		
		long sysTime = System.currentTimeMillis();
		for(int i=0; i<runs; i++){
			interval[i] = runMonteCarlo();
		}
		
		for(int val:interval){
			if(val < minVal) minVal = val;
			if(val > maxVal) maxVal = val;
			sum += val;
		}
		avg = sum/runs;
		long sysTime2 = System.currentTimeMillis();
		System.out.println("Elapsed time: " + (sysTime2 - sysTime)/1000 + " seconds"); //TODO: aufrunden, nicht abrunden...
		
		System.out.println("Interval: [" + minVal + ";" + maxVal + "], avg: " + avg);

	}	
	
	private static int runMonteCarlo(){
		double avgCosts[] = new double[MAX_COPIES];
		double minCosts = Double.MAX_VALUE;
		int minCostsIndex = 0;
		
		for(int numberOfCopies = MIN_COPIES; numberOfCopies<MAX_COPIES; numberOfCopies++){
		
			ArrayList<Long> costsArray = new ArrayList<Long>(); 
	
			for(int run = 0; run<MAX_RUNS; run++){
				double customerCountF = EXPECTATION_VAL + (rnd.nextGaussian()*STD_DEVIATION);
				long customerCountD = Math.round(customerCountF);
				
				long costs;
				long diff = Math.abs(customerCountD - numberOfCopies);
				
				if(customerCountD > numberOfCopies){ //more customers than copies
					costs = diff * TOO_LITTLE_COPIES_COSTS;
				}
				else{ //more or equal copies than customers
					costs = diff * TOO_MUCH_COPIES_COSTS;
				}			
				costsArray.add(costs);
				
			}
			long sum = 0; 
			for(long val : costsArray) sum+=val;
			double avg = ((double)sum)/costsArray.size();	
			avgCosts[numberOfCopies] = avg;
			
			long progress = Math.round((double) (numberOfCopies - MIN_COPIES) / (MAX_COPIES - MIN_COPIES)*100);
			System.out.print("Progress: " +  progress + "%" + "\r");

		}
		//System.out.println("Exemplare;Kosten");
		for(int i= MIN_COPIES; i<avgCosts.length; i++){
			//System.out.println(i + ";" + avgCosts[i]);
			
			if(avgCosts[i] < minCosts){
				minCosts = avgCosts[i];
				minCostsIndex = i;
			}
		}
		
		System.out.println("Minimale Kosten: " + minCosts + ", bei Anzahl Kunden: " + minCostsIndex);
		return minCostsIndex;
	}	
}
